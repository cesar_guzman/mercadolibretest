package mx.mercadolibre.testapp;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by CGUZMANM01 on 9/12/15.
 */
public class Constants {

    public static DisplayImageOptions options;
    public static  MainActivity.AnimateFirstDisplayListener animator;



    public static class Config{
        public  static final boolean DEVELOPER_MODE = false;
    }

}
