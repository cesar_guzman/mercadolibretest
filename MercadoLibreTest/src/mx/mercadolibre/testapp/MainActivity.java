package mx.mercadolibre.testapp;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.impl.AndroidLoggerFactory;

import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import mx.mercadolibre.testapp.fragment.ListResultFragment;

/**
 * Created by CGUZMANM01 on 20/09/15.
 */
public class MainActivity extends AppCompatActivity implements ListResultFragment.OnFragmentInteractionListener{

  private static final org.slf4j.Logger log;

  static {
    AndroidLoggerFactory
        .configureDefaultLogger(MainActivity.class.getPackage());
    log = LoggerFactory.getLogger(MainActivity.class);
  }

  private MenuItem searchItem;
  private SearchRecentSuggestions suggestions;
  private ProgressBar progressBar;
  private SearchView searchView;
  private ListResultFragment resultFragment;
  private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
  private DisplayImageOptions options;

  @Override
  protected void onCreate(Bundle state) {
    log.info("onCreate() intent:{}", getIntent());
    super.onCreate(state);

    setContentView(mx.mercadolibre.testapp.R.layout.layout);

    suggestions = new SearchRecentSuggestions(this,
        SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);

    // Associate searchable configuration with the SearchView
    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    log.debug("onCreateOptionsMenu() searchManager: {}", searchManager);

    searchView = new SearchView(getSupportActionBar().getThemedContext());
    searchView.setSearchableInfo(searchManager
            .getSearchableInfo(getComponentName()));
    searchView.setSubmitButtonEnabled(true);
    searchView.setIconifiedByDefault(true);
    searchView.setMaxWidth(1000);

    progressBar = (ProgressBar) findViewById(R.id.progressBar);


    SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView
        .findViewById(android.support.v7.appcompat.R.id.search_src_text);

    // Collapse the search menu when the user hits the back key
    searchAutoComplete.setOnFocusChangeListener(new OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        log.trace("onFocusChange(): " + hasFocus);
        if (!hasFocus)
          showSearch(false);
      }
    });

   // configureSearchViewOptions();
    initializeImageLoaderDetails();

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    searchItem = menu.add(android.R.string.search_go);


    searchItem.setIcon(mx.mercadolibre.testapp.R.drawable.ic_search_white_36dp);

    MenuItemCompat.setActionView(searchItem, searchView);

    MenuItemCompat.setShowAsAction(searchItem,
            MenuItemCompat.SHOW_AS_ACTION_ALWAYS
                    | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    log.warn("onNewIntent() :{}", intent);
    showSearch(false);
    Bundle extras = intent.getExtras();
    String userQuery = String.valueOf(extras.get(SearchManager.USER_QUERY));
    String query = String.valueOf(extras.get(SearchManager.QUERY));

    log.debug("query: {} user_query: {}", query, userQuery);

    // TODO: Not generate always a Fragment Here.

    resultFragment = ListResultFragment.newInstance(query, userQuery);

    FragmentTransaction transaction = getFragmentManager().beginTransaction();


    transaction.replace(R.id.container_activity, resultFragment);
    transaction.addToBackStack("resultFragment");

    transaction.commit();

  }

  protected void showSearch(boolean visible) {
    if (visible)
      MenuItemCompat.expandActionView(searchItem);
    else
      MenuItemCompat.collapseActionView(searchItem);
  }

  /**
   * Called when the hardware search button is pressed
   */
  @Override
  public boolean onSearchRequested() {
    log.trace("onSearchRequested();");
    showSearch(true);

    // dont show the built-in search dialog
    return false;
  }

  public void configureSearchViewOptions(){
    /*
    findViewById(mx.mercadolibre.testapp.R.id.button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String query = SuggestionProvider.generateRandomSuggestion();

        suggestions.saveRecentQuery("Mi busqueda", "is a nice cheese");
      }
    });

    findViewById(mx.mercadolibre.testapp.R.id.clear_button).setOnClickListener(
            new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                log.debug("clearing suggestions");
                suggestions.clearHistory();
              }
            });

*/
  }


  public void initializeImageLoaderDetails(){
    options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.placeholder)
            .showImageForEmptyUri(R.drawable.ic_empty)
            .showImageOnFail(R.drawable.ic_error)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .build();

    Constants.options = options;
    Constants.animator = (AnimateFirstDisplayListener) animateFirstListener;
  }


  @Override
  public void onFragmentInteraction(String busqueda) {

    suggestions.saveRecentQuery(busqueda, busqueda);
  }

  @Override
  public void onFragmentStartRequest() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onFragmentStopRequest() {
    progressBar.setVisibility(View.INVISIBLE);

  }


  public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
          if (loadedImage != null) {
            ImageView imageView = (ImageView) view;
            boolean firstDisplay = !displayedImages.contains(imageUri);
            if (firstDisplay) {
              FadeInBitmapDisplayer.animate(imageView, 500);
              displayedImages.add(imageUri);
            }
          }
        }
      }
}
