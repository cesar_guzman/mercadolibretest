package mx.mercadolibre.testapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import mx.mercadolibre.testapp.Constants;
import mx.mercadolibre.testapp.R;
import mx.mercadolibre.testapp.dto.Result;

/**
 * Created by CGUZMANM01 on 8/22/15.
 */
public class SearchListAdapter extends BaseAdapter {

   private ArrayList<Result> results;
   private LayoutInflater  inflater;


    public SearchListAdapter(ArrayList<Result> results, Context context){
        this.results = results;
        inflater   = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int i) {
        return results.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        // TODO: Implement ViewHolder Pattern to recycle view

        Result currentResult  = (Result) getItem(i);


        view = inflater.inflate(R.layout.product_list_item, null);

        TextView  textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);

        TextView textViewSubTitle = (TextView) view.findViewById(R.id.textViewSubTitle);
        TextView textViewLocation = (TextView) view.findViewById(R.id.textViewLocation);


        if(currentResult.getPrice()!=null){

            textViewTitle.setText(currentResult.getTitle());
        }


        if(currentResult.getPrice()!=null){
                 textViewSubTitle.setText(currentResult.getPrice().toString());

        }

        if(currentResult.getLocation()!=null){

                 textViewLocation.setText(currentResult.getLocation().getState().getName());

        }

        // Aqui inicia la descarga de la imagen.

        ImageView  imagen  = (ImageView) view.findViewById(R.id.imageView);

        if(currentResult.getThumbnail() !=null){
            ImageLoader.getInstance().displayImage(currentResult.getThumbnail(),
                    imagen,
                    Constants.options,
                    Constants.animator);


        }


        return view;
    }
}
