
package mx.mercadolibre.testapp.dto;


public class Attribute {

    private String attributeGroupId;
    private String id;
    private String name;
    private String valueName;
    private String valueId;
    private String attributeGroupName;

    /**
     * 
     * @return
     *     The attributeGroupId
     */
    public String getAttributeGroupId() {
        return attributeGroupId;
    }

    /**
     * 
     * @param attributeGroupId
     *     The attribute_group_id
     */
    public void setAttributeGroupId(String attributeGroupId) {
        this.attributeGroupId = attributeGroupId;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The valueName
     */
    public String getValueName() {
        return valueName;
    }

    /**
     * 
     * @param valueName
     *     The value_name
     */
    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    /**
     * 
     * @return
     *     The valueId
     */
    public String getValueId() {
        return valueId;
    }

    /**
     * 
     * @param valueId
     *     The value_id
     */
    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    /**
     * 
     * @return
     *     The attributeGroupName
     */
    public String getAttributeGroupName() {
        return attributeGroupName;
    }

    /**
     * 
     * @param attributeGroupName
     *     The attribute_group_name
     */
    public void setAttributeGroupName(String attributeGroupName) {
        this.attributeGroupName = attributeGroupName;
    }

}
