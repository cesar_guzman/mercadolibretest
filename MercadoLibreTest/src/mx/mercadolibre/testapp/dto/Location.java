
package mx.mercadolibre.testapp.dto;


public class Location {

    private String addressLine;
    private String zipCode;
    private Object subneighborhood;
    private Neighborhood neighborhood;
    private City_ city;
    private State_ state;
    private Country_ country;
    private String latitude;
    private String longitude;

    /**
     * 
     * @return
     *     The addressLine
     */
    public String getAddressLine() {
        return addressLine;
    }

    /**
     * 
     * @param addressLine
     *     The address_line
     */
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    /**
     * 
     * @return
     *     The zipCode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * 
     * @param zipCode
     *     The zip_code
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * 
     * @return
     *     The subneighborhood
     */
    public Object getSubneighborhood() {
        return subneighborhood;
    }

    /**
     * 
     * @param subneighborhood
     *     The subneighborhood
     */
    public void setSubneighborhood(Object subneighborhood) {
        this.subneighborhood = subneighborhood;
    }

    /**
     * 
     * @return
     *     The neighborhood
     */
    public Neighborhood getNeighborhood() {
        return neighborhood;
    }

    /**
     * 
     * @param neighborhood
     *     The neighborhood
     */
    public void setNeighborhood(Neighborhood neighborhood) {
        this.neighborhood = neighborhood;
    }

    /**
     * 
     * @return
     *     The city
     */
    public City_ getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    public void setCity(City_ city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The state
     */
    public State_ getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    public void setState(State_ state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The country
     */
    public Country_ getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(Country_ country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
