
package mx.mercadolibre.testapp.dto;

import java.util.ArrayList;
import java.util.List;

public class MercadoLibreSearchApiResponse {

    private String siteId;
    private String query;
    private Paging paging;
    private List<Result> results = new ArrayList<Result>();
    private List<Object> secondaryResults = new ArrayList<Object>();
    private List<Object> relatedResults = new ArrayList<Object>();
    private Sort sort;
    private List<AvailableSort> availableSorts = new ArrayList<AvailableSort>();
    private List<Filter> filters = new ArrayList<Filter>();
    private List<AvailableFilter> availableFilters = new ArrayList<AvailableFilter>();

    /**
     * 
     * @return
     *     The siteId
     */
    public String getSiteId() {
        return siteId;
    }

    /**
     * 
     * @param siteId
     *     The site_id
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * 
     * @return
     *     The query
     */
    public String getQuery() {
        return query;
    }

    /**
     * 
     * @param query
     *     The query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * 
     * @return
     *     The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     * 
     * @param paging
     *     The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    /**
     * 
     * @return
     *     The results
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     * 
     * @param results
     *     The results
     */
    public void setResults(List<Result> results) {
        this.results = results;
    }

    /**
     * 
     * @return
     *     The secondaryResults
     */
    public List<Object> getSecondaryResults() {
        return secondaryResults;
    }

    /**
     * 
     * @param secondaryResults
     *     The secondary_results
     */
    public void setSecondaryResults(List<Object> secondaryResults) {
        this.secondaryResults = secondaryResults;
    }

    /**
     * 
     * @return
     *     The relatedResults
     */
    public List<Object> getRelatedResults() {
        return relatedResults;
    }

    /**
     * 
     * @param relatedResults
     *     The related_results
     */
    public void setRelatedResults(List<Object> relatedResults) {
        this.relatedResults = relatedResults;
    }

    /**
     * 
     * @return
     *     The sort
     */
    public Sort getSort() {
        return sort;
    }

    /**
     * 
     * @param sort
     *     The sort
     */
    public void setSort(Sort sort) {
        this.sort = sort;
    }

    /**
     * 
     * @return
     *     The availableSorts
     */
    public List<AvailableSort> getAvailableSorts() {
        return availableSorts;
    }

    /**
     * 
     * @param availableSorts
     *     The available_sorts
     */
    public void setAvailableSorts(List<AvailableSort> availableSorts) {
        this.availableSorts = availableSorts;
    }

    /**
     * 
     * @return
     *     The filters
     */
    public List<Filter> getFilters() {
        return filters;
    }

    /**
     * 
     * @param filters
     *     The filters
     */
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    /**
     * 
     * @return
     *     The availableFilters
     */
    public List<AvailableFilter> getAvailableFilters() {
        return availableFilters;
    }

    /**
     * 
     * @param availableFilters
     *     The available_filters
     */
    public void setAvailableFilters(List<AvailableFilter> availableFilters) {
        this.availableFilters = availableFilters;
    }

}
