
package mx.mercadolibre.testapp.dto;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private String id;
    private String siteId;
    private String title;
    private Object subtitle;
    private Seller seller;
    private Double price;
    private String currencyId;
    private Integer availableQuantity;
    private Integer soldQuantity;
    private String buyingMode;
    private String listingTypeId;
    private String stopTime;
    private String condition;
    private String permalink;
    private String thumbnail;
    private Boolean acceptsMercadopago;
    private Object installments;
    private Address address;
    private Shipping shipping;
    private SellerAddress sellerAddress;
    private SellerContact sellerContact;
    private Location location;
    private List<Attribute> attributes = new ArrayList<Attribute>();
    private Object originalPrice;
    private String categoryId;
    private Object officialStoreId;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The siteId
     */
    public String getSiteId() {
        return siteId;
    }

    /**
     * 
     * @param siteId
     *     The site_id
     */
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The subtitle
     */
    public Object getSubtitle() {
        return subtitle;
    }

    /**
     * 
     * @param subtitle
     *     The subtitle
     */
    public void setSubtitle(Object subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * 
     * @return
     *     The seller
     */
    public Seller getSeller() {
        return seller;
    }

    /**
     * 
     * @param seller
     *     The seller
     */
    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    /**
     * 
     * @return
     *     The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The currencyId
     */
    public String getCurrencyId() {
        return currencyId;
    }

    /**
     * 
     * @param currencyId
     *     The currency_id
     */
    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * 
     * @return
     *     The availableQuantity
     */
    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    /**
     * 
     * @param availableQuantity
     *     The available_quantity
     */
    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    /**
     * 
     * @return
     *     The soldQuantity
     */
    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    /**
     * 
     * @param soldQuantity
     *     The sold_quantity
     */
    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    /**
     * 
     * @return
     *     The buyingMode
     */
    public String getBuyingMode() {
        return buyingMode;
    }

    /**
     * 
     * @param buyingMode
     *     The buying_mode
     */
    public void setBuyingMode(String buyingMode) {
        this.buyingMode = buyingMode;
    }

    /**
     * 
     * @return
     *     The listingTypeId
     */
    public String getListingTypeId() {
        return listingTypeId;
    }

    /**
     * 
     * @param listingTypeId
     *     The listing_type_id
     */
    public void setListingTypeId(String listingTypeId) {
        this.listingTypeId = listingTypeId;
    }

    /**
     * 
     * @return
     *     The stopTime
     */
    public String getStopTime() {
        return stopTime;
    }

    /**
     * 
     * @param stopTime
     *     The stop_time
     */
    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    /**
     * 
     * @return
     *     The condition
     */
    public String getCondition() {
        return condition;
    }

    /**
     * 
     * @param condition
     *     The condition
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     * 
     * @return
     *     The permalink
     */
    public String getPermalink() {
        return permalink;
    }

    /**
     * 
     * @param permalink
     *     The permalink
     */
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    /**
     * 
     * @return
     *     The thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * 
     * @param thumbnail
     *     The thumbnail
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * 
     * @return
     *     The acceptsMercadopago
     */
    public Boolean getAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    /**
     * 
     * @param acceptsMercadopago
     *     The accepts_mercadopago
     */
    public void setAcceptsMercadopago(Boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    /**
     * 
     * @return
     *     The installments
     */
    public Object getInstallments() {
        return installments;
    }

    /**
     * 
     * @param installments
     *     The installments
     */
    public void setInstallments(Object installments) {
        this.installments = installments;
    }

    /**
     * 
     * @return
     *     The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The shipping
     */
    public Shipping getShipping() {
        return shipping;
    }

    /**
     * 
     * @param shipping
     *     The shipping
     */
    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    /**
     * 
     * @return
     *     The sellerAddress
     */
    public SellerAddress getSellerAddress() {
        return sellerAddress;
    }

    /**
     * 
     * @param sellerAddress
     *     The seller_address
     */
    public void setSellerAddress(SellerAddress sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    /**
     * 
     * @return
     *     The sellerContact
     */
    public SellerContact getSellerContact() {
        return sellerContact;
    }

    /**
     * 
     * @param sellerContact
     *     The seller_contact
     */
    public void setSellerContact(SellerContact sellerContact) {
        this.sellerContact = sellerContact;
    }

    /**
     * 
     * @return
     *     The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * 
     * @param location
     *     The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * 
     * @return
     *     The attributes
     */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * 
     * @param attributes
     *     The attributes
     */
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * 
     * @return
     *     The originalPrice
     */
    public Object getOriginalPrice() {
        return originalPrice;
    }

    /**
     * 
     * @param originalPrice
     *     The original_price
     */
    public void setOriginalPrice(Object originalPrice) {
        this.originalPrice = originalPrice;
    }

    /**
     * 
     * @return
     *     The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * 
     * @param categoryId
     *     The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 
     * @return
     *     The officialStoreId
     */
    public Object getOfficialStoreId() {
        return officialStoreId;
    }

    /**
     * 
     * @param officialStoreId
     *     The official_store_id
     */
    public void setOfficialStoreId(Object officialStoreId) {
        this.officialStoreId = officialStoreId;
    }

}
