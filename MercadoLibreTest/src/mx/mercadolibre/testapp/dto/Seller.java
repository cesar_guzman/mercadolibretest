
package mx.mercadolibre.testapp.dto;

import java.util.ArrayList;
import java.util.List;

public class Seller {

    private Integer id;
    private Object powerSellerStatus;
    private Boolean carDealer;
    private Boolean realEstateAgency;
    private List<Object> tags = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The powerSellerStatus
     */
    public Object getPowerSellerStatus() {
        return powerSellerStatus;
    }

    /**
     * 
     * @param powerSellerStatus
     *     The power_seller_status
     */
    public void setPowerSellerStatus(Object powerSellerStatus) {
        this.powerSellerStatus = powerSellerStatus;
    }

    /**
     * 
     * @return
     *     The carDealer
     */
    public Boolean getCarDealer() {
        return carDealer;
    }

    /**
     * 
     * @param carDealer
     *     The car_dealer
     */
    public void setCarDealer(Boolean carDealer) {
        this.carDealer = carDealer;
    }

    /**
     * 
     * @return
     *     The realEstateAgency
     */
    public Boolean getRealEstateAgency() {
        return realEstateAgency;
    }

    /**
     * 
     * @param realEstateAgency
     *     The real_estate_agency
     */
    public void setRealEstateAgency(Boolean realEstateAgency) {
        this.realEstateAgency = realEstateAgency;
    }

    /**
     * 
     * @return
     *     The tags
     */
    public List<Object> getTags() {
        return tags;
    }

    /**
     * 
     * @param tags
     *     The tags
     */
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

}
