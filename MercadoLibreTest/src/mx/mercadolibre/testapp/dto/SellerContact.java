
package mx.mercadolibre.testapp.dto;

public class SellerContact {

    private String contact;
    private String otherInfo;
    private String areaCode;
    private String phone;
    private String areaCode2;
    private String phone2;
    private String email;
    private String webpage;

    /**
     * 
     * @return
     *     The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * 
     * @param contact
     *     The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * 
     * @return
     *     The otherInfo
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     * 
     * @param otherInfo
     *     The other_info
     */
    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    /**
     * 
     * @return
     *     The areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 
     * @param areaCode
     *     The area_code
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 
     * @return
     *     The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 
     * @param phone
     *     The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 
     * @return
     *     The areaCode2
     */
    public String getAreaCode2() {
        return areaCode2;
    }

    /**
     * 
     * @param areaCode2
     *     The area_code2
     */
    public void setAreaCode2(String areaCode2) {
        this.areaCode2 = areaCode2;
    }

    /**
     * 
     * @return
     *     The phone2
     */
    public String getPhone2() {
        return phone2;
    }

    /**
     * 
     * @param phone2
     *     The phone2
     */
    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The webpage
     */
    public String getWebpage() {
        return webpage;
    }

    /**
     * 
     * @param webpage
     *     The webpage
     */
    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

}
