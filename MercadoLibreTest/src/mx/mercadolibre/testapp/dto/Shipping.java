
package mx.mercadolibre.testapp.dto;


public class Shipping {

    private Boolean freeShipping;
    private String mode;

    /**
     * 
     * @return
     *     The freeShipping
     */
    public Boolean getFreeShipping() {
        return freeShipping;
    }

    /**
     * 
     * @param freeShipping
     *     The free_shipping
     */
    public void setFreeShipping(Boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    /**
     * 
     * @return
     *     The mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * 
     * @param mode
     *     The mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

}
