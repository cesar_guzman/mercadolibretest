
package mx.mercadolibre.testapp.dto;

import java.util.ArrayList;
import java.util.List;
public class Value {

    private String id;
    private String name;
    private List<PathFromRoot> pathFromRoot = new ArrayList<PathFromRoot>();

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The pathFromRoot
     */
    public List<PathFromRoot> getPathFromRoot() {
        return pathFromRoot;
    }

    /**
     * 
     * @param pathFromRoot
     *     The path_from_root
     */
    public void setPathFromRoot(List<PathFromRoot> pathFromRoot) {
        this.pathFromRoot = pathFromRoot;
    }

}
