package mx.mercadolibre.testapp.util;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by CGUZMANM01 on 8/15/15.
 */
public class AsynkHttpRequest extends AsyncTask {

    private HttpRequester requester;

    public AsynkHttpRequest(HttpRequester requester){
        this.requester = requester;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String urlRequest = (String) objects[0];

        HttpUrlClient httpclient = new HttpUrlClient();
        String result = null;
        try {
            result =  httpclient.sendRequest(urlRequest);
        } catch (HttpUrlClientException e) {
            e.printStackTrace();
        }


        return result;
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        requester.onResponseReceived(result.toString());
        Log.d("jSON", result.toString());
    }


    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }
}
