package mx.mercadolibre.testapp.util;

/**
 * Created by CGUZMANM01 on 8/15/15.
 */
public interface HttpRequester {
     void onResponseReceived(String response);
}
