package mx.mercadolibre.testapp.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;



import android.util.Base64;
import android.util.Log;


/**
 * Created by CGUZMANM01 on 8/15/15.
 */

public class HttpUrlClient {
    
   
    
    
    public HttpUrlClient() {
    	
    }
    
    
    /**
     * Inicializa este componente.<br />
     * Precondiciones: Ninguna.
     */
    public void initialize() {}
    
    
    /**
     * Envia una peticion HTTP GET hacia una URL que no requiere autenticacion.
     * @param urlString Cadena que contiene la URL de destino.
     * @return La informacion obtenida de la conexion.
     * @throws HttpUrlClientException Si ocurre algun problema durante la
     * operacion.
     */
    public String sendRequest(String urlString) throws HttpUrlClientException {
        
        Log.i(this.getClass().getName(), "Enviando peticion a " + urlString);
        HttpURLConnection connection = null;
        
        try {
            URL url = new URL(urlString);
            
           
            connection = (HttpURLConnection) url.openConnection();
     
            
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setUseCaches(false);
            
            return readFromConnection(connection);
            
        } catch (IOException e) {
            throw new HttpUrlClientException("Error al abrir conexion.", e);
            
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        
    }
    
    
    /**
     * Envia una peticion HTTP GET hacia una URL que requiere autenticacion.
     * @param urlString Cadena que contiene la URL de destino.
     * @param username Username para autenticacion en la URL.
     * @param password Password para autenticacion en la URL.
     * @return La informacion obtenida de la conexion.
     * @throws HttpUrlClientException Si ocurre algun problema durante la
     * operacion.
     */
    public String sendRequestWithCredentials(String urlString, String username,
            String password)
            throws HttpUrlClientException {
        
    	Log.i(this.getClass().getName(), "Enviando peticion a " + urlString + " con username " +
                username);
        
        HttpURLConnection connection = null;
        
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setUseCaches(false);
            
            String authorization = username + ":" + password;
            String encodedAuthorization =
                    Base64.encodeToString(authorization.getBytes(), Base64.DEFAULT);
            connection.setRequestProperty("Authorization",
                    "basic " + encodedAuthorization);
            
            return readFromConnection(connection);
            
        } catch (IOException e) {
            throw new HttpUrlClientException("Error al abrir conexion.", e);
            
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        
    }
    
    
    /**
     * Lee la informacion que se encuentra disponible en una conexion.
     * @param connection La conexion.
     * @return La informacion leida.
     * @throws HttpUrlClientException Si ocurre algun problema durante la
     * operacion.
     */
    private String readFromConnection(HttpURLConnection connection)
            throws HttpUrlClientException {
        
    	  InputStream inputStream = null;
          InputStreamReader inputStreamReader=null;
          
          try {
              inputStream = connection.getInputStream();
              
               inputStreamReader = new InputStreamReader((InputStream)inputStream, "UTF-8");
              
              
              StringBuilder stringBuilder = new StringBuilder();
              int c;
              while ((c = inputStreamReader.read()) != -1) {
                  stringBuilder.append((char) c);
              }
              
              return stringBuilder.toString();
              
          } catch (IOException e) {
              throw new HttpUrlClientException(
                      "Error al leer de la conexion.", e);
              
          } finally {
              if (inputStreamReader != null) {
                  closeInputStream(inputStream);
	              
                  try {
						inputStreamReader.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
              }
             
          }
    }
    
    
    /**
     * Cierra un {@link InputStream}. Si ocurre algun problema no se arrojan
     * throwables, solo se escribe un mensaje en la salida estandar.
     * @param inputStream El {@link InputStream} que sera cerrado.
     */
    private void closeInputStream(InputStream inputStream) {
        try {
            inputStream.close();
        } catch (Throwable t) {
            Log.e(this.getClass().getName(), "Error al cerrar InputStream"+t);
        }
    }
    
}
