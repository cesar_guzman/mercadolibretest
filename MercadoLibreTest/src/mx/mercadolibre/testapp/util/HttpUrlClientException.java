package mx.mercadolibre.testapp.util;

/**
 * Excepcion checada que indica un error relacionado con el
 * {@link HttpUrlClient}.
 * 

 */
/**
 * Created by CGUZMANM01 on 8/15/15.
 */
public class HttpUrlClientException extends Exception {
    
    private static final long serialVersionUID = 1L;
    
    
    /**
     * Construye una {@link HttpUrlClientException} con el mensaje y la causa
     * especificados.
     * @param message El mensaje de la excepcion.
     * @param cause La causa de la excepcion.
     */
    public HttpUrlClientException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    /**
     * Construye una {@link HttpUrlClientException} con el mensaje especificado.
     * @param message El mensaje de la excepcion.
     */
    public HttpUrlClientException(String message) {
        super(message);
    }
    
    
    /**
     * Construye una {@link HttpUrlClientException} con la causa especificada.
     * @param cause La causa de la excepcion.
     */
    public HttpUrlClientException(Throwable cause) {
        super(cause);
    }
}
